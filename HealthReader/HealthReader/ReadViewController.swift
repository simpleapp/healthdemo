//
//  ViewController.swift
//  HealthReader
//
//  Created by Benjamin Garrigues on 08/04/2016.
//  Copyright © 2016 Simpleapp. All rights reserved.
//

import UIKit
import HealthKit

class ReadViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    let healthKitStore = HKHealthStore()

    override func viewWillAppear(animated: Bool) {
        //TODO: read the latest weight from
        
        if let bodyQuantityType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass) {
            healthKitStore.requestAuthorizationToShareTypes(nil, readTypes: Set(arrayLiteral:bodyQuantityType), completion: { (ok:Bool, error:NSError?) in
                if (ok){
                    
                    let predicate = HKQuery.predicateForSamplesWithStartDate(NSDate.distantPast(), endDate: NSDate(), options: HKQueryOptions.None)
                    let sort = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
                    let query = HKSampleQuery(
                        sampleType: bodyQuantityType,
                        predicate: predicate,
                        limit: 1,
                        sortDescriptors: [sort], resultsHandler: { (query:HKSampleQuery, samples:[HKSample]?, error:NSError?) in
                            
                            
                            
                            if let latestSample = samples?.last as? HKQuantitySample{
                                let weight = latestSample.quantity.doubleValueForUnit(HKUnit.poundUnit());
                                
                                dispatch_async(dispatch_get_main_queue(), {
                                    self.titleLabel.text = "Your weight is \(weight) pounds"
                                })
                            }
                    })
                    
                    self.healthKitStore.executeQuery(query)
                }
            })
        }
        
    }


}

