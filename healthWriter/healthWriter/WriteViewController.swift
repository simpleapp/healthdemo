//
//  ViewController.swift
//  healthWriter
//
//  Created by Benjamin Garrigues on 08/04/2016.
//  Copyright © 2016 Simpleapp. All rights reserved.
//

import UIKit
import HealthKit

class WriteViewController: UIViewController {

    @IBOutlet weak var weightField: UITextField!
    
    @IBOutlet weak var saveButton: UIButton!
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    
    let healthKitStore = HKHealthStore()
    @IBAction func onTapSave(sender: AnyObject) {
        if
            let weightString = weightField.text ,
            let weight = Double(weightString),
            let bodyQuantityType =  HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
            {
                if HKHealthStore.isHealthDataAvailable() {
                    
                    self.healthKitStore.requestAuthorizationToShareTypes(
                        Set(arrayLiteral: bodyQuantityType),
                        readTypes: nil,
                        completion: { (success:Bool, error:NSError?) in
                            
                            if (success){
                                let bodyQuantity = HKQuantity(unit:HKUnit.gramUnitWithMetricPrefix(HKMetricPrefix.Kilo), doubleValue: weight)
                                let bodySample = HKQuantitySample(type:bodyQuantityType, quantity: bodyQuantity, startDate:NSDate(), endDate:NSDate())
                                
                                self.healthKitStore.saveObjects([bodySample], withCompletion: { (success:Bool, error:NSError?) in
                                    dispatch_async(dispatch_get_main_queue(), { 
                                        UIAlertView(title: "success", message: "saved in healthkit", delegate: nil, cancelButtonTitle: "OK").show()
                                    })
                                })
                            }
                    })
                } else {
                    print ("HK not available in this device")
                }
        }
    }
}

